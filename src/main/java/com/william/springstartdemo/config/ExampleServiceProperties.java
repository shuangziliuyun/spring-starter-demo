package com.william.springstartdemo.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author jianweilin
 * @date 2018/11/25
 */
@ConfigurationProperties(prefix = "example.service")
public class ExampleServiceProperties {
    private String prefix;
    private String suffix;

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public String getSuffix() {
        return suffix;
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }
}
