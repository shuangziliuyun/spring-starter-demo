package com.william.springstartdemo.service;

/**
 * @author jianweilin
 * @date 2018/11/25
 */
public class ExampleService {
    private String prefix;
    private String suffix;

    public ExampleService(String prefix, String suffix) {
        this.prefix = prefix;
        this.suffix = suffix;
    }

    public String wrap(String word){
        return String.format("%s%s%s",prefix,word,suffix);
    }
}
